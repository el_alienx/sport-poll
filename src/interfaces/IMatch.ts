import eSports from "./eSports"

export default interface IMatch {
  id: Number,
  objectId: String,
  sport: eSports,
  group: String,
  country: String,
  name: String,
  awayName: String,
  homeName: String,
  createdAt: Date
  state: Boolean
}
