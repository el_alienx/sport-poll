enum eSports {
  FOOTBALL = "FOOTBALL",
  SNOOKER = "SNOOKER",
  HANDBALL = "HANDBALL",
  ICE_HOCKEY = "ICE_HOCKEY",
  TENNIS = "TENNIS"
}

export default eSports