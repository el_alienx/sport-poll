// To improve, use an enum to list the possible vote types

export default interface IUserVote {
  objectId: String,
  voteType: Number
}
