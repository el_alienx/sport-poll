import IUserVote from "@/interfaces/IUserVote"

export default abstract class MatchStore {
  private static _sessionName = "userVotes"

  private static readonly _data = {
    userVotes: Array<IUserVote>()
  }

  // # Pseudo constructor
  public static init() {
    this._loadSession()
  }

  // # API
  public static data() {
    return this._data
  }

  public static isExistingVote(vote: IUserVote): Boolean {
    const userVotes = this._data.userVotes
    const queryVote = userVotes.find(({ objectId }) => objectId === vote.objectId)

    return queryVote !== undefined ? true : false
  }

  // Refactor to utilize isExistingVote
  public static storeVote(newVote: IUserVote) {
    const userVotes = this._data.userVotes
    const queryVote = userVotes.find(({ objectId }) => objectId === newVote.objectId)

    if (queryVote == undefined) {
      userVotes.push(newVote)
    }
    else {
      queryVote.voteType = newVote.voteType
    }

    this._saveSession()
  }

  // # Private methods
  public static _loadSession() {
    const files = sessionStorage.getItem(this._sessionName)
    const newVotes: IUserVote[] = []

    this._data.userVotes = (files !== null) ? JSON.parse(files) : newVotes
  }

  private static _saveSession() {
    const jsonString = JSON.stringify(this._data.userVotes)

    sessionStorage.setItem(this._sessionName, jsonString)
  }
}