import IMatch from '@/interfaces/IMatch';
import eSports from '@/interfaces/eSports';

import Utils from "@/utils/Utils";

export default abstract class MatchStore {

  // # Reactive data
  private static readonly _data = {
    matches: Array<IMatch>(),
    index: 0
  }

  // # Pseudo constructor
  public static init(matches: IMatch[]) {
    this._data.matches = this._filterMatches(matches)
  }

  // # API
  public static data() {
    return this._data
  }

  // # Private methods
  private static _randomizeCategory(): eSports {
    const categories: eSports[] = Object.values(eSports);
    const index: number = Utils.randomizer(categories.length); // move randomize to this store

    return categories[index];
  }

  private static _filterMatches(matches: IMatch[]): IMatch[] {
    const category = this._randomizeCategory();

    return matches.filter(match => match.sport == category);
  }
}